# Kickoff+Grid

Plasma's Kickoff (Application Launcher) menu with favorites in columns/grid.

> **Note:** currently commits/branches are a mess. I will try to fix it at an unspecified time in the future 😫.

## Installation

1. Download and install the plasmoid:

   - (The newest version!) By cloning this repo:

     - and running the `.vscode/run.sh` script.
     - or manually moving the contents of the `package` folder (i.e. `metadata.json` and `contents`) into `~/.local/share/plasma/plasmoids/com.gitlab.cupnoodles14.kickoff/`:

       ```sh
       git clone https://gitlab.com/cupnoodles14/plasma-kickoff-grid
       mv -i plasma-kickoff-grid/package -T ~/.local/share/plasma/plasmoids/com.gitlab.cupnoodles14.kickoff
       rm -rf plasma-kickoff-grid
       ```

   - Or by using "Get New Widgets..." menu.
   - Or by downloading .plasmoid from [KDE Store](https://store.kde.org/p/1317546/) and installing it either by dragging the file to the taskbar, or using "Add Widgets...>Get New Widgets...>Install Widget From Local File..." menus.

2. Switch to it by right-clicking on Application Launcher > Show Alternatives... > Choose Application Launcher (Favorites in a Grid).

## Screenshots

![](https://i.imgur.com/CHGaYBw.png)
![](https://i.imgur.com/XFELlpq.png)
![Options menu](https://i.imgur.com/yxaPu1g.png)

## Todo

- [x] packaging:
  - [ ] update screenshots
  - [x] package and post on kde-store
    - [link](https://store.kde.org/p/1317546/)
  - [ ] translations?
- maybe add more settings (i don't need those though)
  - [x] labels under icons mode
  - [ ] set more compact margins
  - [ ] fixed rows mode
  - [ ] autofit mode
- cleaning up:
  - [ ] check if keyboard controls are fine
  - [ ] make sure scrolling behavior is okay (it had to be reimplemented to support horizontal scrolling without holding down alt)

### Known issues:

- [ ] I fixed the tabs reordering (before, if you changed it the order of tabs, tabs would slide in from the wrong directions) but search tab now slides in from the left side.
- [ ] Rarely, when playing with settings, it fails to set the correct `cellHeight` which could be either too small or too big to fit the gridItem
  - Can't find a reliable way to reproduce it

### I should at least try to submit it upstream, but first...

- [ ] fix known issues
- uncertainties:
  - [ ] If gridView isn't meant to be used on Applications page, there's no point in gridItems having an "arrow" item.
