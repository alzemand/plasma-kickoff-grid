#!/usr/bin/env sh

set -eu

# check if package folder exists
packagefolder=$PWD/package
if [ ! -d "$packagefolder" ]; then
    echo "No package folder is found ($packagefolder)"
    echo "Be sure to run this script from the repo root, not from .vscode folder"
    exit 1
fi

# get name from the metadata.desktop
name=$(kreadconfig5 --file "$PWD"/package/metadata.desktop --group "Desktop Entry" --key "X-KDE-PluginInfo-Name")

# remove old package
# plasmapkg2 -u package
# plasmapkg2 currently can nuke the whole folder while uninstalling,
# removing manually is safer.
# See https://bugs.kde.org/show_bug.cgi?id=410682

rm -rf "$HOME/.local/share/plasma/plasmoids/$name"

# copy package
cp -r "$packagefolder" -T "$HOME/.local/share/plasma/plasmoids/$name"

# apply changes
kbuildsycoca5
kquitapp5 plasmashell && kstart5 plasmashell
