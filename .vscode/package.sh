#!/usr/bin/env sh

tag=$(git describe --abbrev=0 --tags)
zip -r "plasma-kickoff-grid-$tag.plasmoid" package
