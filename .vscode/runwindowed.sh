#!/usr/bin/env sh

name=$(kreadconfig5 --file $(pwd)/package/metadata.desktop --group "Desktop Entry" --key "X-KDE-PluginInfo-Name")
# plasmapkg2 -u package
rm -rf "$HOME/.local/share/plasma/plasmoids/$name"
cp -r package "$HOME/.local/share/plasma/plasmoids/$name"
QT_LOGGING_RULES="qml.debug=true"
plasmawindowed $name
